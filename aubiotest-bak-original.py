#!bin/python

from pyo import *
import os
import random
from vocoder_lib import MyVocoder
# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server()
#s.setMidiInputDevice(3)

#s.setOutputDevice(6)
#s.setOutputDevice(0)
s.boot()
# Drops the gain by 20 dB.
s.amp = 0.1

path_to_aubiofiles = '/home/lou/Music/BBC/07048076'
#path_to_aubiofiles = '/home/lou/Music/BBC/beethoven'
path = '/home/lou/Music/BBC/clip2.wav'

mixval = 0.5


def load_samples_in_tables():
    files = os.listdir(path_to_aubiofiles)
    _sndtables = list()
    for file in files:
        _t = SndTable(os.path.join(path_to_aubiofiles, str(file)), start=0.0, stop=0.25)
        _r = _t.getRate()
        _t.normalize()
        _sndtables.append(_t)
    return _sndtables


def doitman():
    global tr
    global soundtables
    tr.stop()
    tr.setTable(soundtables[random.randint(0, len(soundtables)-1)])
    tr.play()
    #tr.out()

    global trig
    trig = TrigFunc(tr['trig'], doitman, arg=None)



soundtables = load_samples_in_tables()
t = soundtables[random.randint(0, len(soundtables))]

tr = TableRead(t, freq=t.getRate())

tr.out()
tr.play()
tr_hpf = EQ(tr, freq=100, q=0.1, boost=-40.0, type=1, mul=2, add=0)
tr_hpf.ctrl()
#spec_tr = Spectrum(tr_hpf)
vol_tables = Sig(value=0.5)
vol_tables.ctrl(title="VOLUME TABLES")
track_tables = Mix(tr_hpf * vol_tables, 2).out()

tr_delay = Delay(tr, delay=[.125,.650], feedback=.5, mul=.4)

f_lfo = Sine(freq=0.5, mul=50, add=220)
slmap_freq = SLMap(0, 1, "lin", "freq", 0.5)
slmap_mul = SLMap(0, 100, "lin", "mul", 50)
slmap_add = SLMap(100, 500, "lin", "add", 220)

f_lfo.ctrl([slmap_add, slmap_freq, slmap_mul], title="LFO Frequency")

ssine = Sine(freq=f_lfo, mul=1, add=0).out()
sc = Scope(ssine)

#f.ctrl()
f = Phasor(freq=f_lfo, mul=1, add=0)
allp = AllpassWG(tr_delay, freq=f, feed=1, detune=0, mul=.15).out()
allp.ctrl()

# BASSLINE
sinefreq = TrigChoice(tr['trig'], [130.81, 164.81, 196.00]) # , #65.41, 82.41, 98.00
sine = Sine(freq=sinefreq, mul=1, add=0)

sig1 = Sig(value=0.5)
sig1.ctrl(title="VOLUME BASSLINE")
track_bassline = Mix(sine * sig1, 2).out()

# MIDI

mid = Notein(scale=1)
env = MidiAdsr(mid['velocity'], attack=.005, decay=.1, sustain=.4, release=1)
#a = SineLoop(freq=mid['pitch'], feedback=.1, mul=env)
#b = SineLoop(freq=mid['pitch']*1.005, feedback=.1, mul=env)
lfo1 = Sine(.1).range(1, 50)
osc1 = Blit(freq=mid['pitch'], harms=lfo1, mul=0.3)

sig_midi = Sig(value=0.5)
sig_midi.ctrl(title="VOLUME MIDI")
c = Mix([osc1], mul=sig_midi*2).out()

# TWEEDE SAMPLE

# table_sample2 = SndTable('/home/lou/Music/BBC/07070184_clip.wav')
# lfo_sample2 = Phasor()
#
# sig_sample2 = Sig(value=0.5)
# sig_sample2.ctrl(title="VOLUME SAMPLE2")
# osc_sample2 = Granulator(table_sample2, HannTable())
# osc_sample2.ctrl()
#
# track_sample2 = Mix(allp, mul=1).out()

#a = Noise(mul=.7)
#a.ctrl()
#lfo = Sine(freq=0.5, mul=1000, add=1500)
#lfo.ctrl([SLMap(0, 1, "lin", "value", 0.5)], title="Reson Cutoff Frequency")

#f = Resonx(tr, freq=1735.7674, q=11.9378, mul=2.0000).out()
#f.ctrl()

trig = TrigFunc(tr['trig'], doitman, arg=None)

s.gui(locals())

# granulator pitch=1.0000, dur=0.0367, grains=126.0000, basedur=0.2946, mul=1.0000
# freeverb freq=131.3068, feed=1.0000, detune=0.0577, mul=0.1000