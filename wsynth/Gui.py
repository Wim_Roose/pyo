import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class SynthGui(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_default_size(400, 400)
        self.connect("destroy", Gtk.main_quit)
        self.note = 0

        grid = Gtk.Grid()
        self.add(grid)

        value = 220
        adjustment = Gtk.Adjustment(value, 110, 440, 1, 10, 0)

        self.scale = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=adjustment)
        self.scale.set_value_pos(Gtk.PositionType.BOTTOM)
        self.scale.set_vexpand(True)
        self.scale.set_hexpand(True)
        self.scale.connect("value-changed", self.on_scale_slided)
        grid.attach(self.scale, 0, 0, 2, 1)

        hbox = Gtk.Box(spacing=6)
        #self.add(hbox)
        grid.attach(hbox, 0, 1, 2, 1)
        button1 = Gtk.RadioButton.new_with_label_from_widget(None, "C")
        button1.connect("toggled", self.on_button_toggled, "C")
        hbox.pack_start(button1, False, False, 0)

        button2 = Gtk.RadioButton.new_from_widget(button1)
        button2.set_label("F")
        button2.connect("toggled", self.on_button_toggled, "F")
        hbox.pack_start(button2, False, False, 0)

        button3 = Gtk.RadioButton.new_from_widget(button2)
        button3.set_label("G")
        button3.connect("toggled", self.on_button_toggled, "G")
        hbox.pack_start(button3, False, False, 0)

        button4 = Gtk.RadioButton.new_from_widget(button3)
        button4.set_label("A")
        button4.connect("toggled", self.on_button_toggled, "A")
        hbox.pack_start(button4, False, False, 0)

    def on_scale_slided(self, scale):
        # print (scale.get_value())
        scale.get_value()

    def on_button_toggled(self, button, name):
        notes={"C": 0, "D": 2, "E": 4, "F": 5, "G": 7, "A": 9, "B": 10}
        #print(button.get_label())
        #print(notes[str(button.get_label())])
        self.note = notes[str(button.get_label())]
        #exit()

    def on_buttonF_toggled(self, button, name):
        global NOTE
        NOTE=10
        print("toggle")

    def get_frequency(self):
        # print (scale.get_value())
        return self.scale.get_value()

    def get_note(self):
        return self.note

    def run(self):
        Gtk.main()
