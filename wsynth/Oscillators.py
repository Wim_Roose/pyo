#!/usr/bin/env python

import numpy as np
import time


class Oscillator(object):
    def __init__(self, buffer_length=1024, frequency=440, sample_rate=44100):
        self._bl = buffer_length
        self._f = frequency
        self._sr = sample_rate
        self._ts = None
        self._phase = 0

    @property
    def phase(self):
        return self._phase

    @property
    def f(self):
        return self._f

    @f.setter
    def f(self, frequency):
        self._f = frequency


class Sine(Oscillator):
    def __init__(self,buffer_length=1024, frequency=440, sample_rate=44100):
        super(Sine, self).__init__( buffer_length=buffer_length, frequency=frequency, sample_rate=sample_rate )

    def get_buffer(self):

        # ts = one cycle
        self._ts = np.arange(float(self._bl+1))/float(self._sr)

        phases = (np.pi*2 * self._f * self._ts + self._phase)
        sig = np.sin(phases[:-1])

        # !!!! startfase van eerste sample van volgende loop moet eigenlijk de fase zijn
        # van de volgende sample (die nog niet is berekend. Zie code van Joren)
        self._phase = phases[self._bl]

        # versie van Joren
        # self.phase = np.pi*2 * self.f * self.buffer_length / self.sr + self.phase
        return sig.astype(np.float32)


class Square(Oscillator):
    def __init__(self, buffer_length=1024, frequency=440, sample_rate=44100):
        super(Square, self).__init__(buffer_length=buffer_length, frequency=frequency, sample_rate=sample_rate)

    def get_buffer(self):
        # ts = one cycle
        self._ts = np.arange(float(self._bl + 1)) / float(self._sr)

        phases = np.pi * 2 * self._f * self._ts + self._phase
        sig = np.sign(np.cos(phases[:-1]))

        # !!!! startfase van eerste sample van volgende loop moet eigenlijk de fase zijn
        # van de volgende sample (die nog niet is berekend. Zie code van Joren)
        self._phase = np.sign(phases[self._bl])

        # versie van Joren
        # self.phase = np.pi*2 * self.f * self.buffer_length / self.sr + self.phase
        return sig.astype(np.float32)

    