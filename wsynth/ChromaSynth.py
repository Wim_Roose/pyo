#!/usr/bin/env python


import numpy as np
import random
import os


class SamplePlayer:

    def __init__(self, sf, wl=2048, hop=512):
        self._note = 0
        self._wl = wl
        self._sf = sf
        self._hop = hop
        self._chromas = None
        self._wd = None
        self._chrd = None
        #print(self._chromas.shape)
        self._y = None
        self._counter = 0
        self._position = 0
        self._previous_random = 0
        self.pointer = 0
        self.previous_note = 0

    def get_random_sample(self, note=0, dir=0):
        #if not self.previous_note == note:
        #    self._counter = 4
        #    self.previous_note = note
        # print(len(self._chromas[note]))
        # print(len(self._y))
        if self._counter == 1:
            _random = random.randint(0, len(self._chromas[note]) - 1)
            self._previous_random = _random
            self._counter = 0
        else:
            _random = self._previous_random

        begin_pos = (self._hop*self._chromas[note][_random])-(self._hop//2)
        # print(begin_pos)
        end_pos = (begin_pos + self._wl)
        # print(end_pos)
        # print(self._y[begin_pos:end_pos])
        # print(self._y)
        self._counter = self._counter + 1
        if dir == 0:
            return begin_pos / 44100, end_pos / 44100
        else:
            return end_pos / 44100, begin_pos / 44100
        #sample = self._y[begin_pos:end_pos]
        #_max = np.amax(sample)
        #return sample * 1/_max

    def get_next_sample(self, note=0):
        print("Chroma array length for note %d: %d" % (note, len(self._chromas[note])))
        print(self._chromas[note])
        # print(len(self._y))

        if self.pointer == len(self._chromas[note]):
        #if self.pointer == 10:
            return None
        # begin_pos = (self._hop*self._chromas[note][self.pointer])-(self._wl//2)
        begin_pos = self._hop * self._chromas[note][self.pointer]
        # print(begin_pos)
        end_pos = (begin_pos + self._hop)
        # print(end_pos)
        # print(self._y[begin_pos:end_pos])
        # print(self._y)
        self._counter = self._counter + 1
        self.pointer = self.pointer + 1
        sample = self._y[begin_pos:end_pos]
        _max = np.amax(sample)
        return sample * 1/_max

    def set_working_dirs(self, working_dir = None, chroma_dir = None):
        self._wd = working_dir
        self._chrd = chroma_dir
        self._chromas = np.array([np.load(os.path.join(self._wd, self._chrd, self._sf + '-chroma-' + str(i) + '.npy')) for i in range(12)])
        


