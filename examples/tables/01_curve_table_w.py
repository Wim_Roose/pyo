#!/usr/bin/env python
# encoding: utf-8
"""
Curve table variations used as an amplitude envelope.

"""
from pyo import *

s = Server(duplex=0).boot()

#[(0,0.0000),(937,0.1646),(3991,0.1463),(4078,0.3110),(6144,0.0183),(8191,1.0000)]
# Curvetable points. min 0, max 8182 - 1
#t = CurveTable([(0,0.0000),(4200,0.4451),(8191,1.0000)], tension=0, bias=20).normalize()
#t = LinTable([(0,0.0000),(0,0.4939),(1076,0.3841),(2152,0.5122),(2811,0.1463),(3228,0.5122),(4096,0.1768),(4616,0.5000),(5276,0.1951),(5796,0.4817),(7063,0.5976),(8192,0.1890)],).normalize()
t = LogTable([(0,0.0000),(0,0.4939),(1076,0.3841),(2152,0.5122),(2811,0.1463),(3228,0.5122),(4096,0.1768),(4616,0.5000),(5276,0.1951),(5796,0.4817),(7063,0.5976),(8192,0.1890)],).normalize()


t.graph(title="LFO shape")

# create LFO OSC. Frequentie: 0.5 / s -> ganse table = 2 seconden.
# mul: 220 -> van -220 tot +220, add=440 -> alle fluctuaties boven 440Hz
lfo1 = Osc(table=t, freq=0.5, mul=220, add=220)

m = Metro(4).play()
tr = TrigEnv(m, table=t, dur=4)
f = SumOsc(freq=[301,300], ratio=[.2498,.2503], index=tr, mul=.2).out()

# synth1 = BrownNoise(a).mix(2).out()
# synth2 = FM(carrier=[100,50], ratio=[.495,1.01], index=10, mul=a).out()

#sine1 = Sine(freq=lfo1, mul=0.5).mix(2).out()



# # LFO from 0 to 20
# c = Sine(.1, 0, 10, 10)

# # Modifying the bias parameter 10 times per second
# def change():
#     # get the current value of the LFO
#     val = c.get()
#     t.setBias(val)
#     t.normalize()
#     t.refreshView()

# p = Pattern(change, .1).play()

s.gui(locals())
