"""
Demo script for showing how GUI classes from pyo can be used to build
audio programs with graphical interface.

"""
import wx, time, random
from pyo import *

NCHNLS = 2

server = Server(nchnls=NCHNLS).boot()
server.start()

### A basic audio process ###
snd = SndTable([SNDS_PATH+"/transparent.aif"]*NCHNLS)
m = Metro(.125, poly=1).play()
am = Iter(m, [1,0,0,0]*4)
t2 = ExpTable([(0,1),(4096,10),(8191,10)], exp=4)
q = TrigEnv(m, t2, m.time)
pos = TrigLinseg(m, [(0.0,0.0), (m.time, 1,0)])
n = Pointer(snd, pos, mul=am)
fr = SigTo(1000, time=0.05, init=1000)
f = ButBP(n, freq=fr, q=q).out()
#pa = PeakAmp(f)
sp = Spectrum(f)
sc = Scope(f)

class MyFrame(wx.Frame):

    def __init__(self, parent, title, pos=(50, 50), size=(1000, 600)):
        wx.Frame.__init__(self, parent, -1, title, pos, size)
        
        self.Bind(wx.EVT_CLOSE, self.on_quit)

        self.panel = wx.Panel(self)
        vmainsizer = wx.BoxSizer(wx.VERTICAL)
        mainsizer = wx.BoxSizer(wx.HORIZONTAL)
        leftbox = wx.BoxSizer(wx.VERTICAL)
        midbox = wx.BoxSizer(wx.VERTICAL)
        rightbox = wx.BoxSizer(wx.VERTICAL)

        ### PyoGuiControlSlider - logarithmic scale ###
        sizer1 = self.createFreqSlider()

        leftbox.Add(sizer1, 0, wx.ALL | wx.EXPAND, 5)
        
        mainsizer.Add(leftbox, 1, wx.ALL | wx.EXPAND, 5)
        self.panel.SetSizerAndFit(vmainsizer)

    def on_quit(self, evt):
        server.stop()
        time.sleep(0.25)
        self.Destroy()

    
    def createFreqSlider(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        label = wx.StaticText(self.panel, -1, "PyoGuiControlSlider: filter's center frequency (log scale)")
        sizer.Add(label, 0, wx.CENTER|wx.ALL, 5)
        self.freq = PyoGuiControlSlider(parent=self.panel,
                                        minvalue=20,
                                        maxvalue=20000,
                                        init=1000,
                                        pos=(0, 0),
                                        size=(200, 16),
                                        log=True,
                                        integer=False,
                                        powoftwo=False,
                                        orient=wx.HORIZONTAL)
        #print(self.freq.getRange())
        #print(self.freq.isPowOfTwo())
        self.freq.Bind(EVT_PYO_GUI_CONTROL_SLIDER, self.changeFreq)
        sizer.Add(self.freq, 0, wx.ALL | wx.EXPAND, 5)
        return sizer

    def changeFreq(self, evt):
        fr.value = evt.value

app = wx.App(False)
mainFrame = MyFrame(None, title='Test Pyo GUI objects')
mainFrame.Show()
app.MainLoop()
