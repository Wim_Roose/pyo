from pyo import *

s = Server(duplex=0)
ins = pm_get_input_devices()
print(ins)
pm_list_devices()
s.setMidiInputDevice(3)
s.boot()

# MIDI

mid = Notein(scale=1)
env = MidiAdsr(mid['velocity'], attack=.005, decay=.1, sustain=.4, release=1)
a = Sine(freq=mid['pitch'], mul=env).mix(2).out()


s.gui(locals())

# s = Server()
# s.setMidiInputDevice(3)
# s.boot()
# s.start()
# notes = Notein(poly=10, scale=1, mul=.5)
# p = Port(notes['velocity'], .001, .5)
# b = Sine(freq=notes['pitch'], mul=p).out()
# c = Sine(freq=notes['pitch'] * 0.997, mul=p).out()
# d = Sine(freq=notes['pitch'] * 1.005, mul=p).out()
# s.gui(locals())