#!bin/python

from pyo import *
import random
from wsynth import ChromaSynth

# VARIABLES
# length of sample being requested
buffer_length = 2048
mixval = 0.5
# initial (chroma) note of sample
note = 0
# path to wav file that has been chroma analyzed
path = '/home/lou/Music/wibrosa/Dee_Yan-Key_-_18_-_Good_Bye.wav'
path = '/home/lou/Music/wibrosa/bartok-allegrobarbaro-helling.wav'
# Drops the gain by 20 dB.
amp = 0.1
counter=0
prefix='synthsample_'
ext='.wav'

# initialize Chromasynth
sample = '/home/lou/Music/wibrosa/stream.wav'

# initialize Chromasynth
chromasynth = ChromaSynth.SamplePlayer('bartok-allegrobarbaro-helling.wav', wl=buffer_length)
chromasynth.set_working_dirs('/home/lou/Music/wibrosa', 'chromas')

s = Server()
s.boot()
s.amp = amp


def get_sample(st):
    #global tr
    #print(type(info))
    #exit(0)

    _begin, _end = chromasynth.get_random_sample(note=note, dir = 0)
    # forward
    st.setSound(path, start=_begin, stop=_end)


def get_sample_reversed(st):
    #global tr
    #print(type(info))
    #exit(0)
    _begin, _end = chromasynth.get_random_sample(note=note, dir=0)
    # forward
    st.setSound(path, start=_begin, stop=_end)
    st.reverse()

# Table for the Triangle envelope generator
triEnvTable = CosTable([(0,0.0000),(1145,0.3148),(2481,0.6914),(4100,0.9500),(6352,0.5556),(7150,0.2840),(8070,0.0000),(8191,0.0000)])

triEnvTable.graph()
begin, end = chromasynth.get_random_sample(note=note)
sndtable1 = SndTable(sample, start=begin, stop=end)
sndtable2 = SndTable(sample, start=begin, stop=end)
tr1 = TableRead(sndtable1, freq=sndtable1.getRate(), loop=1).play()
tr2 = TableRead(sndtable2, freq=sndtable2.getRate(), loop=1).play()

trigger1 = tr1['trig']
trigger2 = tr2['trig']

# u1 = TrigFunc(trigger1, get_sample, arg=(sndtable1, 0))
u1 = TrigFunc(trigger1, get_sample, arg=sndtable1)
u2 = TrigFunc(trigger2, get_sample_reversed, arg=sndtable2)

env_table = HannTable()
_amp1 = TrigEnv(trigger1, triEnvTable, dur=sndtable1.getDur(), mul=1)
_amp2 = TrigEnv(trigger2, triEnvTable, dur=sndtable2.getDur(), mul=1)

tr1.mul = _amp1*10
tr1.mul = _amp1*10

mono2stereo1 = Pan(tr1, outs=2, pan=0.5)

delay = SDelay(mono2stereo1, delay=(buffer_length/2)/44100, mul=1).out(1)
mono2stereo2 = Pan(delay, outs=2, pan=0.5)

Compress(mono2stereo1, thresh=-20, ratio=4, risetime=.005, falltime=.1, knee=0.5, mul=2).mix(2).out()
Compress(mono2stereo2, thresh=-20, ratio=4, risetime=.005, falltime=.1, knee=0.5, mul=2).mix(2).out()


sine = Sine(freq=[130.6,131.6], mul=1).out()

s.gui(locals())
