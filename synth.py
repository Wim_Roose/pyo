#!bin/python

from pyo import *
from wsynth import ChromaSynth

# VARIABLES
# length of sample being requested
buffer_length = 2048
mixval = 0.5
# initial (chroma) note of sample
note = 0
# path to wav file that has been chroma analyzed
path = '/home/lou/Music/wibrosa/Dee_Yan-Key_-_18_-_Good_Bye.wav'

# initialize Chromasynth
chromasynth = ChromaSynth.SamplePlayer('Dee_Yan-Key_-_18_-_Good_Bye.wav', wl=buffer_length)
chromasynth.set_working_dirs('/home/lou/Music/wibrosa', 'chromas')

# Creates and boots the pyo server.
# The user should send the "start" command from the GUI.
s = Server()
#s.setMidiInputDevice(3)
#s.setOutputDevice(6)
#s.setOutputDevice(0)
s.boot()
# Drops the gain by 20 dB.
s.amp = 0.1


def get_sample():
    global tr
    global sndtable
    # stop Tableread, request sample & load it in Sndtable, and reload Tablereader
    tr.stop()
    _begin, _end = chromasynth.get_random_sample(note=note)
    sndtable.setSound(path, start=_begin, stop=_end)
    tr.setTable(sndtable)
    # relaunch play
    tr.play()


begin, end = chromasynth.get_random_sample(note=note)
sndtable = SndTable(path, start=begin, stop=end)
tr = TableRead(sndtable, freq=sndtable.getRate())
tr.play()
p = Pan(input=tr, outs=2, pan=0.5, spread=0.0)
p.out()
trig = TrigFunc(tr['trig'], get_sample, arg=None)

s.gui(locals())

