#!bin/python

from pyo import *
import os
# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server().boot()
# Drops the gain by 20 dB.
s.amp = 0.1

sine = Sine(freq=400, mul=1, add=0)

sig1 = Sig(value=0.5)
sig1.ctrl()
#mulsine = sine * sig1.value
mix_1 = Mix(sine * sig1, 2).out()


s.gui(locals())
