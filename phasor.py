#!bin/python
from pyo import *

s = Server().boot()
s.start()
f = Phasor(freq=[1, 1.5], mul=1000, add=500)
sine = Sine(freq=f, mul=.2).out()
s.gui(locals())
