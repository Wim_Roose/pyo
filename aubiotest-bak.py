#!bin/python

from pyo import *
import os
import random
from vocoder_lib import MyVocoder
# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server().boot()
# Drops the gain by 20 dB.
s.amp = 0.1

path_to_aubiofiles = '/home/lou/Music/BBC/07048076'
#path_to_aubiofiles = '/home/lou/Music/BBC/beethoven'
path = '/home/lou/Music/BBC/clip2.wav'

mixval = 0.5

def load_samples_in_tables():
    files = os.listdir(path_to_aubiofiles)
    _sndtables = list()
    for file in files:
        _t = SndTable(os.path.join(path_to_aubiofiles, str(file)), start=0.0, stop=0.25)
        _r = _t.getRate()
        _t.normalize()
        _sndtables.append(_t)
    return _sndtables


def doitman():
    global tr
    global soundtables
    #print('Got trigger')
    tr.stop()
    tr.setTable(soundtables[random.randint(0, len(soundtables)-1)])
    tr.play()
    tr.out()

    global trig
    trig = TrigFunc(tr['trig'], doitman, arg=None)



soundtables = load_samples_in_tables()
t = soundtables[random.randint(0, len(soundtables))]

tr = TableRead(t, freq=t.getRate())

tr.out()
tr.play()

sinefreq = TrigChoice(tr['trig'], [130.81, 110, 164.81])
sine = Sine(freq=sinefreq, mul=1, add=0)

# mm = Mixer(outs=2, chnls=2, time=.025).out()
# mm.addInput(0, tr)
# mm.addInput(1, sine)
# sig1 = Sig(value=0.1)
# sig1.ctrl(title='Tables track volume')
# mm.setAmp(0, 0, sig1.get())
#
# sig2 = Sig(value=0.5)
# sig2.ctrl(title='Bass track volume')
# mm.setAmp(1, 0, Sig(0.5))

sig1 = Sig(value=0.5)
sig1.ctrl(title='Bass track volume')
m = mix_1 = Mix(sine, 2).out()



#a = Noise(mul=.7)
#a.ctrl()
#lfo = Sine(freq=0.5, mul=1000, add=1500)
#lfo.ctrl([SLMap(0, 1, "lin", "value", 0.5)], title="Reson Cutoff Frequency")

#f = Resonx(tr, freq=1735.7674, q=11.9378, mul=2.0000).out()
#f.ctrl()

trig = TrigFunc(tr['trig'], doitman, arg=None)

s.gui(locals())