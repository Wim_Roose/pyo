"""
02-linear-ramp.py - Portamento, glissando, ramping.

The SigTo object allows to create audio glissando between the
current value and the target value, within a user-defined time.
The target value can be a float or another PyoObject. A new ramp
is created everytime the target value changes.

Also:

The VarPort object acts similarly but works only with float and
can call a user-defined callback when the ramp reaches the target
value.

The PyoObject.set() method is another way create a ramp for any
given parameter that accept audio signal but is not already
controlled with a PyoObject.

"""
from pyo import *
import sys, os 
sys.path.append(os.getcwd()) 

s=Server()
print(s)
pa_list_devices()
s.setOutputDevice(7)
s.boot()
#s.start()
#s.stop()


# Bassline 1
notes = midiToHz([60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71])
#envelope = Adsr(attack=0.01, decay=0.1, sustain=0.5, release=1.5, dur=2, mul=0.5)
envelope = LinTable([(0,0), (128, 1), (512, 0.7), (4096, 0.7), (8192, 0)])
metro = Metro(0.046439909).play()
# On each trig, pick the next note in the list...
fr = Iter(metro, notes)
# ... and start reading the envelope.
amp = TrigEnv(metro, envelope, dur=0.046439909, mul=0.7)

# Sine wave with given frequency and amplitude envelope.
sig = Sine(freq=fr, mul=amp).out()

# Creates the recorders
brec = Record(sig, filename='testtones.wav', chnls=1, fileformat=0, sampletype=0)
clean = Clean_objects(1.5, brec)

# Starts the internal timer of Clean_objects. Use its own thread.
clean.start()

s.start()

s.gui(locals())
