"""
02-linear-ramp.py - Portamento, glissando, ramping.

The SigTo object allows to create audio glissando between the
current value and the target value, within a user-defined time.
The target value can be a float or another PyoObject. A new ramp
is created everytime the target value changes.

Also:

The VarPort object acts similarly but works only with float and
can call a user-defined callback when the ramp reaches the target
value.

The PyoObject.set() method is another way create a ramp for any
given parameter that accept audio signal but is not already
controlled with a PyoObject.

"""

import sys, os 
sys.path.append(os.getcwd()) 

s=Server()
print(s)
s.boot()
s.start()
s.stop()
amp=SigTo(value=0.3, time=2.0, init=0.0)
pick=Choice([500,1000,10,10], freq=8)
pp=Print(pick, method=1, message="Frequency")
freq=SigTo(pick, time=0.01, mul=[1, 1.005])
freq.ctrl([SLMap(0, 0.5, "lin", "time", 0.01, dataOnly=True)])
sig=RCOsc(freq, sharp=0.3, mul=amp).out()
from mymodule import test
test.lekker2(s)