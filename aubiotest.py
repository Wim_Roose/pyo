#!bin/python

from pyo import *
import os
import random

# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server().boot()
# Drops the gain by 20 dB.
s.amp = 0.1

path_to_aubiofiles = '/home/lou/Music/BBC/07048076'
path = '/home/lou/Music/BBC/clip2.wav'


def load_samples_in_tables():
    files = os.listdir(path_to_aubiofiles)
    _tables = list()
    for file in files:
        _t = SndTable(os.path.join(path_to_aubiofiles, str(file)), start=0.0, stop=0.25)
        _r = _t.getRate()
        _tables.append(TableRead(_t, freq=_r))
    return _tables


def doitman():
    global t
    print('Got trigger')
    t.stop()
    t = tables[random.randint(0, len(tables))]
    t.play()
    t.out()

    global trig
    trig = TrigFunc(t['trig'], doitman, arg=None)


tables = load_samples_in_tables()
t = tables[random.randint(0, len(tables))]

#TrigChoice(input, choice, port=0.0, init=0.0, mul=1, add=0)


t.out()
t.play()
trig = TrigFunc(t['trig'], doitman, arg=None)

freq = Sig(1000)
freq.ctrl([SLMap(50, 5000, "lin", "value", 1000)], title="Cutoff Frequency")

# Three different lowpass filters
tone = Tone(t, freq)
butlp = ButLP(t, freq)
mooglp = MoogLP(t, freq)

# Interpolates between input objects to produce a single output
sel = Selector([tone, butlp, mooglp]).out()
sel.ctrl(title="Filter selector (0=Tone, 1=ButLP, 2=MoogLP)")

s.gui(locals())