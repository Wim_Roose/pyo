#!bin/python

from pyo import *

# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server().boot()
# Drops the gain by 20 dB.
s.amp = 0.1

path = '/home/lou/Music/BBC/clip2.wav'

t = SndTable(path)

freq = t.getRate()
print(freq)

# Mul van beide LFO's mag niet te veel verschillen, anders maar 1 effect
lfo1 = Sine(freq=3, mul=1, add=0)
lfo1.ctrl(title="LFO AMP", map_list=[SLMap(0.1, 10, 'log', 'freq', 3)])

lfo2 = Sine(freq=5, mul=1, add=0)
lfo2.ctrl(title="LFO PITCH", map_list=[SLMap(1, 20, 'log', 'freq', 5)])

osc = Osc(table=t, freq=0.8, phase=[0, 0.5], mul=lfo1).out()

osc.ctrl(map_list=[SLMap(0, 1, 'lin', 'freq', 0.5)])

s.gui(locals())