#!bin/python

from pyo import *

# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server().boot()
# Drops the gain by 20 dB.
s.amp = 0.1

path = '/home/lou/Music/BBC/clip.wav'

t = SndTable(path)

freq = t.getRate()

lfo1 = Sine(freq=3, mul=1, add=0)

osc = Osc(table=t, freq=0.8, phase=[0, 0.5], mul=lfo1).out()

osc.ctrl([SLMapFreq(3)])

s.gui(locals())