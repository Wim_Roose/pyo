"""
02-linear-ramp.py - Portamento, glissando, ramping.

The SigTo object allows to create audio glissando between the
current value and the target value, within a user-defined time.
The target value can be a float or another PyoObject. A new ramp
is created everytime the target value changes.

Also:

The VarPort object acts similarly but works only with float and
can call a user-defined callback when the ramp reaches the target
value.

The PyoObject.set() method is another way create a ramp for any
given parameter that accept audio signal but is not already
controlled with a PyoObject.

"""
from pyo import *
import sys, os 
sys.path.append(os.getcwd()) 

s=Server()
print(s)
pa_list_devices()
s.setOutputDevice(7)
s.boot()
s.start()
#s.stop()

# Saw
pick=Choice([100,200,400], freq=4)
freq=SigTo(pick, time=0.5)
lfo1 = Sine(freq=.5, mul=10, add=freq)
a = SuperSaw(freq=lfo1, mul=0.5).out()

# Bassline 1
notes = midiToHz([60, 0, 60, 0, 60, 0, 65, 0])
#envelope = Adsr(attack=0.01, decay=0.1, sustain=0.5, release=1.5, dur=2, mul=0.5)
envelope = LinTable([(0,0), (128, 1), (512, 0.7), (4096, 0.7), (8192, 0)])
metro = Metro(0.25).play()
# On each trig, pick the next note in the list...
fr = Iter(metro, notes)
# ... and start reading the envelope.
amp = TrigEnv(metro, envelope, dur=0.25, mul=0.3)

# Sine wave with given frequency and amplitude envelope.
sig = Sine(freq=fr, mul=amp).out()

# Bassline 2
b2notes = midiToHz([0, 72, 0, 72, 0, 72, 0, 72])
#envelope = Adsr(attack=0.01, decay=0.1, sustain=0.5, release=1.5, dur=2, mul=0.5)
b2envelope = LinTable([(0,0), (256, 1), (512, 0.7), (4096, 0.7), (8192, 0)])
b2metro = Metro(time=0.5).play()
# On each trig, pick the next note in the list...
b2fr = Iter(b2metro, b2notes)
# ... and start reading the envelope.
b2amp = TrigEnv(b2metro, b2envelope, dur=0.5, mul=0.3)

# Sine wave with given frequency and amplitude envelope.
b2sig = Sine(freq=b2fr, mul=b2amp).out()




amp=SigTo(value=0.3, time=2.0, init=0.0)
pick=Choice([500,1000,10,10], freq=8)
pp=Print(pick, method=1, message="Frequency")
freq=SigTo(pick, time=0.01, mul=[1, 1.005])
freq.ctrl([SLMap(0, 0.5, "lin", "time", 0.01, dataOnly=True)])
sig=RCOsc(freq, sharp=0.3, mul=amp).out()
from mymodule import test
test.lekker2(s)

s.gui(locals())
