[TOC]

[TOC]

# PYO

### mono to stereo

> mono2stereo = Pan(tr, outs=2, pan=0.5).out()

* [forum](https://groups.google.com/forum/#!msg/pyo-discuss/5kHyMMwsXso/8MMk2l4LL8oJ;context-place=msg/pyo-discuss/HtSDiWYzwmw/eH9NjrVOdAEJ)

### Metro

```
t = CosTable([(0,0), (50,1), (250,.3), (8191,0)])
t.graph()
met = Metro(time=.5, poly=1).play()
amp = TrigEnv(met, table=t, dur=1, mul=.3)
comp = Compress(grn, thresh=-20, ratio=4, risetime=0.005, falltime=0.10, knee=0.5, mul=amp*5).out()
```
