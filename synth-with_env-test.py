#!bin/python

from pyo import *
import random
from wsynth import ChromaSynth

# VARIABLES
# length of sample being requested
buffer_length = 4096
mixval = 0.5
# initial (chroma) note of sample
note = 0
# path to wav file that has been chroma analyzed
path = '/home/lou/Music/wibrosa/Dee_Yan-Key_-_18_-_Good_Bye.wav'

# initialize Chromasynth
sample = '/home/lou/Music/wibrosa/stream.wav'
# initialize Chromasynth
chromasynth = ChromaSynth.SamplePlayer('Dee_Yan-Key_-_18_-_Good_Bye.wav', wl=buffer_length)
chromasynth.set_working_dirs('/home/lou/Music/wibrosa', 'chromas')
# Creates and boots the pyo server.
# The user should send the "start" command from the GUI.
s = Server()
#s.setMidiInputDevice(3)
#s.setOutputDevice(6)
#s.setOutputDevice(0)
s.boot()
# Drops the gain by 20 dB.
s.amp = 0.1
counter=0
prefix='synthsample_'
ext='.wav'
sample_length=8192*5


def get_sample():
    #global tr
    global sndtable
    global counter
    # stop Tableread, request sample & load it in Sndtable, and reload Tablereader
    #tr.stop()
    _begin, _end = chromasynth.get_random_sample(note=note)
    sndtable.setSound(path, start=_begin, stop=_end)

    #tr.setTable(sndtable)
    filename = "%s%d%s" % (prefix, counter, ext)
    #print(filename)
    #savefileFromTable(sndtable, filename, fileformat=0, sampletype=0, quality=1)
    # relaunch play
    counter = counter + 1

    #tr.play()


# Table for the Triangle envelope generator
triEnvTable = CosTable([(0,0.0000),(1787,0.5864),(4100,0.9500),(6890,0.5741),(8191,0.0000)])

triEnvTable.graph()
begin, end = chromasynth.get_random_sample(note=note)
sndtable = SndTable(sample, start=begin, stop=end)
tr = TableRead(sndtable, freq=sndtable.getRate(), loop=1).play()

trigger = tr['trig']
u=TrigFunc(trigger, get_sample, arg=None)
#p = Pan(input=tr, outs=2, pan=0.5, spread=0.0)
#p.out()
#a = Looper(sndtable*env, pitch=[1.,1.], dur=sndtable.getDur(), xfade=5, mul=1, mode=1).out()
#triggerFunction = TrigFunc(trigger, get_sample, arg=None)
env_table = HannTable()
_amp = TrigEnv(trigger, triEnvTable, dur=sndtable.getDur(), mul=1)

tr.mul = _amp*10
#tr.out()
mono2stereo = Pan(tr, outs=2, pan=0.5).out()

sine = Sine(freq=[130.6,131.6], mul=1).out()

s.gui(locals())
