#!bin/python

from pyo import *

# Creates and boots the server.
# The user should send the "start" command from the GUI.
s = Server().boot()
# Drops the gain by 20 dB.
s.amp = 0.1

tone1_freq = 400


def pp(address, *args):
    print(address)
    print(args)


r = OscReceive(9900, address=['/tone1/pitch'])

path1 = '/home/lou/Music/BBC/clip2.wav'
path2 = '/home/lou/Music/BBC/clip.wav'

t1 = SndTable(path1)
t2 = SndTable(path2)

freq1 = t1.getRate()
freq2 = t2.getRate()
print(freq1)
print(freq2)

# Mul van beide LFO's mag niet te veel verschillen, anders maar 1 effect
lfo1 = Sine(freq=3, mul=1, add=0)
lfo1.ctrl(title="LFO AMP", map_list=[SLMap(0.1, 10, 'log', 'freq', 3)])

lfo2 = Sine(freq=5, mul=1, add=0)
lfo2.ctrl(title="LFO PITCH", map_list=[SLMap(1, 20, 'log', 'freq', 5)])

osc1 = Osc(table=t1, freq=0.8, phase=[0, 0.5], mul=lfo1).out()
osc1.ctrl(map_list=[SLMap(0, 1, 'lin', 'freq', 0.5)])

osc2 = Osc(table=t2, freq=0.8, phase=[0, 0.5], mul=lfo1).out()
osc2.ctrl(map_list=[SLMap(0, 1, 'lin', 'freq', 0.5)])

tone1 = Sine(freq=r['/tone1/pitch'], mul=0.5, add=0)
tone1.out()

s.gui(locals())