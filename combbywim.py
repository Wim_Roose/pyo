from pyo import *
import random

s = Server(duplex=0)
ins = pm_get_input_devices()
print(ins)
pm_list_devices()
s.setMidiInputDevice(3)
s.boot()


#n = Noise(.1).mix(2).out()

path_to_aubiofiles = '/home/lou/Music/BBC/07048076'
#path_to_aubiofiles = '/home/lou/Music/BBC/07055129'

#path_to_aubiofiles = '/home/lou/Music/BBC/beethoven'
path = '/home/lou/Music/BBC/clip2.wav'

mixval = 0.5



def load_samples_in_tables():
    files = os.listdir(path_to_aubiofiles)
    _sndtables = list()
    for file in files:
        _t = SndTable(os.path.join(path_to_aubiofiles, str(file)), start=0.0, stop=0.25)
        _r = _t.getRate()
        _t.normalize()
        _sndtables.append(_t)
    return _sndtables


def doitman():
    global tr
    global soundtables
    tr.stop()
    tr.setTable(soundtables[random.randint(0, len(soundtables)-1)])
    tr.play()
    tr.out()
    #tr.out()

    global trig
    trig = TrigFunc(tr['trig'], doitman, arg=None)


soundtables = load_samples_in_tables()
t = soundtables[random.randint(0, len(soundtables))]

tr = TableRead(t, freq=t.getRate())


tr.play()
tr.out()

freq1 = 220
freq2 = freq1 * 2
# 200 / s
#freqs = midiToHz([60,62,64,65,67,69,71,72])

freqs = [1/220, 1/440, 1/130.81, 1/261.6]
met = Metro(.125, poly=2).play()
freq = TrigChoice(met, freqs)

pfreq = Print(freq, method=1, message="Frequency")

d1 = Delay(tr, delay=freq, feedback=.9, mul=1)


#d1.out()
#d1.ctrl()

trig = TrigFunc(tr['trig'], doitman, arg=None)

# MIDI

mid = Notein(scale=1)
env = MidiAdsr(mid['velocity'], attack=.005, decay=.1, sustain=.4, release=1)
a = Sine(freq=mid['pitch'], mul=env).out()


s.gui(locals())